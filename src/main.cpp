#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

#define NEOPIXEL_PIN 15
#define NEOPIXEL_NUM 1
#define LED_PIN 5
#define BUTTON_PIN 4

#define BLINK_TIME_SHORT 100
#define BLINK_TIME_LONG 4000

#define MIC_VOLUME_MAX 300

#define MAX_POINTS 255
#define MAX_G_COUNT 2
#define MAX_FREQ 31

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NEOPIXEL_NUM, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);


// ADXL345 I2C address is 0x53(83)
//#define Addr 0x53

const float timeStep = 1./64;
const int timeStepMillsec = 1000/64;
const int arrSize = 64;

int pos = 0; // Start position for write axelerate. In stable phase = 64.
float xAcc[64]; // Axelerates to x in the last second
float yAcc[64]; // Axelerates to y in the last second
float zAcc[64]; // Axelerates to z in the last second

double xFreq, xAmpl;
double yFreq, yAmpl;
double zFreq, zAmpl;

static double frequencies[64];

int maxFreqIndex;
double amplitudeMaxFreq;

unsigned int data[6];

int point = 0;
long pointAmount = 0;

float xAccl;
float yAccl;
float zAccl;

    unsigned long prevMillAccRec = 0;
    unsigned long prevMillFreqCalc = 0;
    unsigned long prevMillPointPay = 0;
    unsigned long prevMillShow = 0;

// AVal - массив анализируемых данных, Nvl - длина массива должна быть кратна степени 2.
// FTvl - массив полученных значений, Nft - длина массива должна быть равна Nvl.

// Start For FFT Analise
const double TwoPi = 6.283185307179586;

int i, j, n, m, Mmax, Istp;
double Tmpr, Tmpi, Wtmp, Theta;
double Wpr, Wpi, Wr, Wi;
double Tmvl[128];
// End

 
void displaySensorDetails(void)
{
 sensor_t sensor;
 accel.getSensor(&sensor);
 Serial.println("------------------------------------");
 Serial.print ("Sensor: "); Serial.println(sensor.name);
 Serial.print ("Driver Ver: "); Serial.println(sensor.version);
 Serial.print ("Unique ID: "); Serial.println(sensor.sensor_id);
 Serial.print ("Max Value: "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
 Serial.print ("Min Value: "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
 Serial.print ("Resolution: "); Serial.print(sensor.resolution); Serial.println(" m/s^2"); 
 Serial.println("------------------------------------");
 Serial.println("");
 delay(500);
}
 
void displayDataRate(void)
{
 Serial.print ("Data Rate: "); 
 
 switch(accel.getDataRate())
 {
 case ADXL345_DATARATE_3200_HZ:
 Serial.print ("3200 "); 
 break;
 case ADXL345_DATARATE_1600_HZ:
 Serial.print ("1600 "); 
 break;
 case ADXL345_DATARATE_800_HZ:
 Serial.print ("800 "); 
 break;
 case ADXL345_DATARATE_400_HZ:
 Serial.print ("400 "); 
 break;
 case ADXL345_DATARATE_200_HZ:
 Serial.print ("200 "); 
 break;
 case ADXL345_DATARATE_100_HZ:
 Serial.print ("100 "); 
 break;
 case ADXL345_DATARATE_50_HZ:
 Serial.print ("50 "); 
 break;
 case ADXL345_DATARATE_25_HZ:
 Serial.print ("25 "); 
 break;
 case ADXL345_DATARATE_12_5_HZ:
 Serial.print ("12.5 "); 
 break;
 case ADXL345_DATARATE_6_25HZ:
 Serial.print ("6.25 "); 
 break;
 case ADXL345_DATARATE_3_13_HZ:
 Serial.print ("3.13 "); 
 break;
 case ADXL345_DATARATE_1_56_HZ:
 Serial.print ("1.56 "); 
 break;
 case ADXL345_DATARATE_0_78_HZ:
 Serial.print ("0.78 "); 
 break;
 case ADXL345_DATARATE_0_39_HZ:
 Serial.print ("0.39 "); 
 break;
 case ADXL345_DATARATE_0_20_HZ:
 Serial.print ("0.20 "); 
 break;
 case ADXL345_DATARATE_0_10_HZ:
 Serial.print ("0.10 "); 
 break;
 default:
 Serial.print ("???? "); 
 break;
 } 
 Serial.println(" Hz"); 
}
 
void displayRange(void)
{
 Serial.print ("Range: +/- "); 
 
 switch(accel.getRange())
 {
 case ADXL345_RANGE_16_G:
 Serial.print ("16 "); 
 break;
 case ADXL345_RANGE_8_G:
 Serial.print ("8 "); 
 break;
 case ADXL345_RANGE_4_G:
 Serial.print ("4 "); 
 break;
 case ADXL345_RANGE_2_G:
 Serial.print ("2 "); 
 break;
 default:
 Serial.print ("?? "); 
 break;
 } 
 Serial.println(" g"); 
}

void FFTAnalysis(float *AVal, double *FTvl, int Nvl, int Nft) {
    n = Nvl * 2;

    for (i = 0; i < n; i += 2) {
        Tmvl[i] = 0;
        Tmvl[i + 1] = AVal[i / 2];
    }

    i = 1; j = 1;
    while (i < n) {
        if (j > i) {
            Tmpr = Tmvl[i]; Tmvl[i] = Tmvl[j]; Tmvl[j] = Tmpr;
            Tmpr = Tmvl[i + 1]; Tmvl[i + 1] = Tmvl[j + 1]; Tmvl[j + 1] = Tmpr;
        }
        i = i + 2; m = Nvl;
        while ((m >= 2) && (j > m))
            j = j - m; m = m >> 1;
        
        j = j + m;
    }

    Mmax = 2;
    while (n > Mmax) {
        Theta = -TwoPi / Mmax; Wpi = sin(Theta);
        Wtmp = sin(Theta / 2); Wpr = Wtmp * Wtmp * 2;
        Istp = Mmax * 2; Wr = 1; Wi = 0; m = 1;

        while (m < Mmax) {
            i = m; m = m + 2; Tmpr = Wr; Tmpi = Wi;
            Wr = Wr - Tmpr * Wpr - Tmpi * Wpi;
            Wi = Wi + Tmpr * Wpi - Tmpi * Wpr;

            while (i < n) {
                j = i + Mmax;
                Tmpr = Wr * Tmvl[j] - Wi * Tmvl[j - 1];
                Tmpi = Wi * Tmvl[j] + Wr * Tmvl[j - 1];

                Tmvl[j] = Tmvl[i] - Tmpr; Tmvl[j - 1] = Tmvl[i - 1] - Tmpi;
                Tmvl[i] = Tmvl[i] + Tmpr; Tmvl[i - 1] = Tmvl[i - 1] + Tmpi;
                i = i + Istp;
            }
        }

        Mmax = Istp;
    }

    for (i = 0; i < Nft; i++) 
    {    j = i * 2; FTvl[i] = 2 * sqrt(pow(Tmvl[j], 2) + pow(Tmvl[j + 1], 2)) / Nvl;
    }

} 

void accelerateRecording(){
    static sensors_event_t event; 
    accel.getEvent(&event);    

    xAccl = event.acceleration.x;
    yAccl = event.acceleration.y;
    zAccl = event.acceleration.z;

    if(pos == arrSize){
        for(int i = 1; i < arrSize; i++){ // Shift Arrays
            xAcc[i-1] = xAcc[i];
            yAcc[i-1] = yAcc[i];
            zAcc[i-1] = zAcc[i];
        }
        xAcc[arrSize - 1] = xAccl; 
        yAcc[arrSize - 1] = yAccl;
        zAcc[arrSize - 1] = zAccl;
    }
    else{
        xAcc[pos] = xAccl; 
        yAcc[pos] = yAccl;
        zAcc[pos] = zAccl;
        if(pos==0){
            for(i = 0; i < arrSize; i++)
            {
                xAcc[i] = xAccl;
                yAcc[i] = yAccl;
                zAcc[i] = zAccl;
            }
        }
        pos++;
    }

}

void freqCalc(){
    xFreq = 0;
    xAmpl = 0;
    
    FFTAnalysis(xAcc, frequencies, arrSize, arrSize);
    for(i = 0; i < arrSize / 2; i++)
        if(frequencies[i] > xAmpl){
            xFreq = i;
            xAmpl = frequencies[i];
        }

    yFreq = 0;
    yAmpl = 0;
    FFTAnalysis(yAcc, frequencies, arrSize, arrSize);
    for(i = 0; i < arrSize / 2; i++)
        if(frequencies[i] > yAmpl){
            yFreq = i;
            yAmpl = frequencies[i];
        }

    zFreq = 0;
    zAmpl = 0;
    FFTAnalysis(zAcc, frequencies, arrSize, arrSize);
    for(i = 0; i < arrSize / 2; i++)
        if(frequencies[i] > zAmpl){
            zFreq = i;
            zAmpl = frequencies[i];
        }
}

void pointsPayer(double xAmpl, double yAmpl, double zAmpl, double xFreq, double yFreq, double zFreq){
    point = 0;
    //if(xAmpl > 30 || yAmpl > 30 || zAmpl > 30){
        if(xAmpl > yAmpl && xAmpl > zAmpl){
            point = round(min(abs(xFreq * xAmpl) * MAX_POINTS / (MAX_FREQ * MAX_G_COUNT * 9.8), 255.0));
        }
        if(yAmpl > xAmpl && yAmpl > zAmpl){
            point = round(min(abs(yFreq * yAmpl) * MAX_POINTS / (MAX_FREQ * MAX_G_COUNT * 9.8), 255.0));
        }
        if(zAmpl > xAmpl && zAmpl > yAmpl){
            point = round(min(abs(yFreq * yAmpl) * MAX_POINTS / (MAX_FREQ * MAX_G_COUNT * 9.8), 255.0));
        }
        pointAmount += point;
    //}
}

//ssid: FeerBOX-8a63dd
//password: w1458208

char* ssid     = "FeerBOX-xxxxxx";
char* password = "xxxxxxxx";

int httpServerErrorsCount = 0;

String macString = "000000";

const char* host = "192.168.4.1";

char *deviceid = "xxxxxx";

int micZeroOffset = 0;
boolean boxFound = 0;

unsigned long prevBlinkTime = 0;
int blinkCounter = 1;
int blinkTimePause = BLINK_TIME_SHORT;

enum { red, green, blue, yellow, white, purple };

int valuesFromMic[10] = {0,0,0,0,0,0,0,0,0,0};
int currentValueFromMicNumber = 0;

WiFiClient client;
HTTPClient http;

int value = 0;

void neopixelInit(){
  pinMode(NEOPIXEL_PIN,OUTPUT);
  strip.begin();
  strip.show();
}

void ledInit(){
  pinMode(LED_PIN,OUTPUT);
}

void fireUpLed(){
  digitalWrite(LED_PIN, LOW);
}

void shutdownLed(){
  digitalWrite(LED_PIN, HIGH);
}

void toggleLed(){
  digitalWrite(LED_PIN,!(digitalRead(LED_PIN)));
}

void neopixelSetColor(int color){
  switch(color){
    case red:
      strip.setPixelColor(0, strip.Color(100, 0, 0));
      strip.setPixelColor(1, strip.Color(100, 0, 0));
      strip.setPixelColor(2, strip.Color(100, 0, 0));
      break;
    case green:
      strip.setPixelColor(0, strip.Color(0, 100, 0));
      strip.setPixelColor(1, strip.Color(0, 100, 0));
      strip.setPixelColor(2, strip.Color(0, 100, 0));
      break;
    case blue:
      strip.setPixelColor(0, strip.Color(0, 0, 100));
      strip.setPixelColor(1, strip.Color(0, 0, 100));
      strip.setPixelColor(2, strip.Color(0, 0, 100));
      break;
    case yellow:
      strip.setPixelColor(0, strip.Color(90, 90, 0));
      strip.setPixelColor(1, strip.Color(90, 90, 0));
      strip.setPixelColor(2, strip.Color(90, 90, 0));
      break;
    case white:
      strip.setPixelColor(0, strip.Color(100, 100, 100));
      strip.setPixelColor(1, strip.Color(100, 100, 100));
      strip.setPixelColor(2, strip.Color(100, 100, 100));
      break;
    case purple:
      strip.setPixelColor(0, strip.Color(90, 0, 90));
      strip.setPixelColor(1, strip.Color(90, 0, 90));
      strip.setPixelColor(2, strip.Color(90, 0, 90));
      break;
  }
  strip.show();
}

void neopixelTurnOff(){
  strip.setPixelColor(0, strip.Color(0, 0, 0));
  strip.setPixelColor(1, strip.Color(0, 0, 0));
  strip.setPixelColor(2, strip.Color(0, 0, 0));
  strip.show();
}

void setChipId(){
  String flashIdString = (String)ESP.getFlashChipId();
  for (int i=0; i < 6; i++){
    deviceid[i] = flashIdString[i];
  }
}

void generatePasswordFromSSID(){
  password[0] = char(int(macString[2])/2+48);
  password[1] = char(int(macString[1])/2+33);
  password[2] = char(int(macString[0])/2+60);
  password[3] = char(int(macString[0])/2+25);
  password[4] = char(int(macString[4])/2+29);
  password[5] = char(int(macString[5])/2+53);
  password[6] = char(int(macString[3])/2+57);
  password[7] = char(int(macString[3])/2+44);
  for (int pwi = 0; pwi < 8; pwi++){
    if  (password[pwi] > ':' && password[pwi] < '@') password[pwi] -= 7;
  }
  Serial.print("password: ");
  Serial.println(password);
}


int calibrateMicrophone(){
  unsigned long calibratingMeasures = 0;
  for (int cmi = 0; cmi <=100; cmi++){

    calibratingMeasures += analogRead(A0);
    delay(15);

    delay(15);
  }
  return calibratingMeasures/100;
}

void sendValueOverWiFi(int _value){
  byte valueToSend = byte(_value);
  http.begin("http://192.168.4.1/input?t=2&v=" + String(valueToSend) + "&i=" + String(deviceid));
  Serial.println("http://192.168.4.1/input?t=2&v=" + String(valueToSend) + "&i=" + String(deviceid));
  Serial.println(http.getString());
  int httpCode = http.GET();
  if(httpCode > 0) {
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);
      if(httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        Serial.println(payload);
      }
  }
  else{
    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    httpServerErrorsCount++;
  }
  http.end();

}

void initialiseASDX345(){
 if(!accel.begin())
 {
 /* There was a problem detecting the ADXL345 ... check your connections */
 Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
 while(1);
 }
 
 /* Set the range to whatever is appropriate for your project */
 accel.setRange(ADXL345_RANGE_16_G);
 // displaySetRange(ADXL345_RANGE_8_G);
 // displaySetRange(ADXL345_RANGE_4_G);
 // displaySetRange(ADXL345_RANGE_2_G);
 
 /* Display some basic information on this sensor */
 displaySensorDetails();
 
 /* Display additional settings (outside the scope of sensor_t) */
 displayDataRate();
 displayRange();
 Serial.println("");
}

void initialiseFreq(){
    for(int i = 0; i < arrSize; i++)
        frequencies[i] = 0;
}

void initialiseWIFI(){
  neopixelInit();
  setChipId();
  //Serial.begin(115200);
  //Serial.begin(9600);
  delay(10);
  ledInit();


  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  while(!boxFound){

    fireUpLed();
    Serial.println("scanning");
    int n = WiFi.scanNetworks();

    if (n == 0) {
      Serial.println(F("no networks found"));
    } else {
      for (int i = 0; i < n; ++i) {

        delay(10);
        if (WiFi.SSID(i).substring(0,7) == "FeerBOX"){
          Serial.println(F("Found our BOX!"));
          boxFound = 1;
          macString = WiFi.SSID(i).substring(8,14);
          for (int sai = 0; sai < 6; sai++){
            ssid[sai+8] = WiFi.SSID(i)[sai+8];
          }
          Serial.print("SSID: ");
          Serial.println(ssid);
          generatePasswordFromSSID();
          break;

        }
      }
      Serial.println(F("No BOX found"));
      delay(1000);
    }
  }

  shutdownLed();

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    fireUpLed();
    delay(100);
    Serial.print(".");
    shutdownLed();
    delay(100);
  }

  client.setNoDelay(1);

  Serial.println("");
  Serial.println(F("WiFi connected"));
  Serial.println(F("IP address: "));
  Serial.println(WiFi.localIP());

  if (client.connect("192.168.4.1", 80)) {
    Serial.println(F("Connected"));
  } else {
    Serial.println(F("Connection failed."));
    while(1) {
      // Hang on failure
    }
  }

  fireUpLed();
  micZeroOffset = calibrateMicrophone();
  shutdownLed();
}

void setup() {
  Serial.begin(9600);

  initialiseASDX345();
  initialiseFreq();

  initialiseWIFI();
}

void loop() {
  if (httpServerErrorsCount == 10)ESP.restart();


  if (millis() - prevBlinkTime > blinkTimePause){
    toggleLed();
    blinkCounter++;
    if ((blinkCounter % 4) == 0) blinkTimePause = BLINK_TIME_LONG;
    else blinkTimePause = BLINK_TIME_SHORT;
    prevBlinkTime = millis();
  }

  // value = analogRead(A0);
  // Serial.print("RAW: "); Serial.print(value);
  // value = constrain(value-micZeroOffset, 0, 1023);
  // Serial.print(" off: "); Serial.print(value);
  // value = map(value, 0, MIC_VOLUME_MAX-micZeroOffset, 0, 64);
  // Serial.print(" map: "); Serial.println(value);

  // valuesFromMic[currentValueFromMicNumber] = value;
  // currentValueFromMicNumber += 1;
  // if (currentValueFromMicNumber == 10){
  //   value = 0;
  //   currentValueFromMicNumber = 0;
  //   for(int vfmi =0; vfmi < 10; vfmi++){
  //     value += valuesFromMic[vfmi];
  //   }
  //   value /= 10;
  //   sendValueOverWiFi(value);
  // }

  //   delay(10);


    if(millis() - prevMillAccRec > timeStepMillsec){
        prevMillAccRec = millis();
       accelerateRecording();
    }

    if(millis() - prevMillFreqCalc > timeStepMillsec){
        prevMillFreqCalc = millis();
        freqCalc();
    }

    if(millis() - prevMillPointPay > timeStepMillsec){
        prevMillPointPay = millis();
        pointsPayer(xAmpl, yAmpl, zAmpl, xFreq, yFreq, zFreq);
    }

    if(millis() - prevMillShow > 100){
       prevMillShow = millis();
       Serial.print("Points: ");
       Serial.println(point);
       sendValueOverWiFi(point);
    }
    
}



 

